Algorytm oceny wielkości tafli diamentu

Opis rozwiązania

Algorytm przystosowany jest do działania na zdjęciach kamieni szlachetnych oszlifowanych wg. klasycznego szlifu brylantowego (w szczególności szlifu Hearts&Arrows), wykonanych z góry, ukazujących taflę kamienia pod kątem prostopadłym do osi widzenia. Celem algorytmu jest lokalizacja oraz pomiar wielkości tafli diamentu, która definiowana jest jako stosunek średnicy tafli kamienia, stanowiącej sześciokątną, górną powierzchnię kamienia do średnicy rondysty, a więc obwodu kamienia.
